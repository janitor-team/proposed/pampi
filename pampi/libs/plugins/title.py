"""
Un plugin destiné à Pampi, pour insérer un titre dans l'éditeur
à la position courante du curseur.
"""

try:
    from PyQt5 import QtCore, QtWidgets, QtGui
except:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui

import os, re

from ._template import Plugin as Template

from .plugin_rc import *
from .ui_title import Ui_Dialog

title_pattern = re.compile(r""".*""", re.M|re.S)
title_template = """{header} {title}"""

level_pattern = re.compile("^(#*)(.*)")

class TitleDialog(QtWidgets.QDialog, Ui_Dialog):
    def __init__(self, parent = None, level = 1):
        super(TitleDialog, self).__init__(parent)
        self.level = 1
        self.setupUi(self)
        # on assigne des valeurs aux boutons radio
        # et on les connecte à leur fonction de rappel
        for i in range(1,6):
            button = self.getButton(i)
            def levelChoiceMaker(val):
                def callback(checked):
                    if checked:
                        self.level = val
                    return
                return callback
            button.toggled.connect(levelChoiceMaker(i))
        return

    def getButton(self, i):
        """
        renvoie le i-ème bouton
        """
        return getattr(self, "radioButton_{}".format(i))
        
class Plugin(Template):
    """
    Le constructeur prend comme paramètre la fenêtre principale
    de l'application. Il doit définir les propriété name et action
    @param mainWindow la fenêtre pincipale
    """
    def __init__(self, mainWindow):
        iconPath = ':/img/icons/title.png'
        title = QtCore.QCoreApplication.translate("main","Make a title")
        self.parent = mainWindow
        Template.__init__(self, mainWindow,
                          name = "title", iconPath = iconPath, title = title)
        return
        
    def _run(self):
        """
        Fonction de rappel pour le plugin
        """
        fragment = self.getSelected()
        m = title_pattern.search(fragment)
        if m:
            # on réunit toute la zone sélectionnée en une seule ligne
            fragment = " ".join([l.strip() for l in fragment.split("\n")])
            if fragment == "":
                fragment = QtCore.QCoreApplication.translate("main", "Title")
            hashtags, title = level_pattern.match(fragment).groups()
            level = len(hashtags)-1
            if level <= 0:
                level = 1
            if level > 5:
                level = 5
            self.td = TitleDialog(self.parent, level = level)
            self.td.contentsEdit.setText(title)
            button = self.td.getButton(level)
            button.setChecked(True)
            ok = self.td.exec_()
        if ok:
            hashtags = "#"*(self.td.level +1)
            title = self.td.contentsEdit.text()
            if len(title.strip()) == 0:
                title = "..."
            self.insertSelected(
                title_template.format(header = hashtags, title=title))
        return
        
    
