"""
Un plugin destiné à Pampi, pour insérer une animation dans l'éditeur
à la sélection courante du curseur. La classe Plugin concerne une 
"oscillation-Y"
"""

try:
    from PyQt5 import QtCore, QtWidgets, QtGui
except:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui


from ._anim_template import Plugin as AnimPlugin, AnimDialog
from .plugin_rc import *

class Plugin(AnimPlugin):
    def __init__(self, parent):
        AnimPlugin.__init__(
            self, parent,
            iconPath = ':/img/icons/rotation-y-infinite.png',
            title = QtCore.QCoreApplication.translate("main","Make an oscillation around Y axis"),
            name = QtCore.QCoreApplication.translate("main","Oscillate Y"),
            animation_type = "anim-rotate-y-infinite"
        )
        return
        
