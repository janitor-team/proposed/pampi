<!--
HEADER FOR PAMPI CONFIG
TITLE:JSXGraph
COLOR:#f1f6ff
OTHER TOOLS:|JSXGraph
-->


#  {.step data-x=-2000 data-y=-2000 data-scale=4}

## TEST D'OUTILS JS











# {.step data-x=0 data-y=1000}
## JSXGraph

[http://jsxgraph.uni-bayreuth.de/wp/index.html](http://jsxgraph.uni-bayreuth.de/wp/index.html)


# {.step .slide data-x=1000 data-y=1000}

<div id="jsxgraph-1" class="jxgbox" style="width: 100%; height: 450px;"></div>
<script>
var board = JXG.JSXGraph.initBoard('jsxgraph-1', {boundingbox: [-10, 10, 10, -10], axis:true});
//board.create('point', [1,3], {name:'A', strokecolor:'red'});

var p1 = board.create('point',[-1,1], {name:'A',size:4});
var p2 = board.create('point',[2,-1], {name:'B',size:4});
var p3 = board.create('point',[4,4], {name:'C',size:4});
var li1 = board.create('line',["A","B"], {straightFirst:false, straightLast:false, strokeColor:'#00ff00',strokeWidth:2});
var li2 = board.create('line',["A","C"], {straightFirst:false, straightLast:false, strokeColor:'#00ff00',strokeWidth:2});
var li3 = board.create('line',["B","C"], {straightFirst:false, straightLast:false, strokeColor:'#00ff00',strokeWidth:2});
</script>





# {.step .slide data-x=2000 data-y=1000}

<div id="jsxgraph-2" class="jxgbox" style="width: 500px; height: 500px;"></div>
<script>
(function() {
var b2 = JXG.JSXGraph.initBoard('jsxgraph-2', {boundingbox: [-5, 5, 5, -5]});
var p1 = b2.create('point', [-2,-2], {name:'', size:4}),
    p2 = b2.create('point', [3,3], {name:'', size:4}),
    pm = b2.create('midpoint', [p1, p2], {visible: false}),
    circ = b2.create('circle', [pm, p1], {visible: true}),
    p3 = b2.create('glider', [-2, 3, circ], {color:'green',name:'', size:4}),
    t = b2.create('transform', [Math.PI, pm], {type: 'rotate'}),
    p4 = b2.create('point', [p3, t], {color:'blue', name:'', size:1}),
    rect = b2.create('polygon',[p1,p3,p2,p4],{color:'blue', hasInnerPoints:false});
})();
</script>



<!--
# {.step data-x=3000 data-y=1000}
## 9
![](data/pampi-help/splash.png)


# {.step data-x=4000 data-y=1000}
## 10
![](data/pampi-help/splash.png)
-->


















# {#overview .step data-x=0 data-y=0 data-scale=10}
