# PAMPI
## <small>Presentations With Markdown, Pandoc, Impress.</small>

----

* **Website:** http://pascal.peter.free.fr/pampi.html
* **Email:** pascal.peter at free.fr
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 2017-2021

----

### The tools used to develop PAMPI

* [Python](https://www.python.org)
* [PyQt](https://riverbankcomputing.com)
* [marked](https://github.com/markedjs/marked)
* [MarkdownHighlighter](https://github.com/rupeshk/MarkdownHighlighter)

### and those used for presentations

* [Markdown](https://daringfireball.net/projects/markdown)
* [Pandoc](http://www.pandoc.org)
* [impress.js](https://github.com/impress/impress.js)
* [Bootstrap](https://getbootstrap.com)

